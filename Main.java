import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int[] array = new int[8];
        for (int i = 0; i < 8; i++){
            array[i] = random.nextInt(9) + 1;
        }
        for (int i : array){
            System.out.print(i + " ");
        }
        boolean t = true;
        for (int i = 1 ; i < 8 ; i++){
            if (array[i]<=array[i-1]) {
                t = false;
                break;
            }
        }
        if (t == true) System.out.println("\nЯвляется строго возрастающей последовательностью");
        else System.out.println("\nНе является строго возрастающей последовательностью");
        for (int i = 1; i < 8; i+=2){
            array[i] = 0;
        }
        for (int i : array){
            System.out.print(i + " ");
        }
    }
}
